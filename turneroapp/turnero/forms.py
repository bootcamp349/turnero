from django import forms
from .models import Cliente, Persona, Cola


class ClienteModelForm(forms.ModelForm):
    class Meta:
        model= Cliente
        fields=["persona_id", "prioridad_id"]

class ClienteFormulario(forms.Form):
    # aqui podemos hacer el formulario independientemente del models.py
    # lo recomendable es hacerlo a partir del modelo, para poder persistir
    # datos correctos en la bd
    # el admin de django crea a partir del models, un formulario con validaciones
    # asi como esta configurado nuestro modelo
    # utilizando una clase modelForm, podemos manipular ese formulario
    persona_id = forms.IntegerField()
    prioridad_id = forms.IntegerField()

class PersonaModelForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ["nombre_razon_social","numero_documento","tipo_documento", "email", "hora_insercion","sexo"]

    def clean_email(self):
        email = self.cleaned_data.get("email") #no parece reconocer .get
        email_base, proveedor = email.split("@")#no parece reconocer .split
        aux = proveedor.split(".")
        dominio = aux[0]
        extension = aux[1]
        if not extension=="com":
            raise forms.ValidationError("Por favor, ingrese un email con extencion .com")
        return email


class ColaModelForm(forms.ModelForm):
    class Meta:
        model = Cola
        fields = ["usuario_insercion","cliente","hora_insercion", "estado", "servicio"]


class PersonaFormulario(forms.Form):
    nombre_razon_social = forms.CharField(max_length=25)
    numero_documento = forms.CharField(max_length=11)
    tipo_documento = forms.IntegerField()
    email = forms.EmailField(max_length=100)
    hora_insercion = forms.DateField()
    sexo = forms.IntegerField()