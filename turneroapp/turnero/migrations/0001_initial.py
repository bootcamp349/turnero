# Generated by Django 4.1.7 on 2023-03-14 21:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'cargo',
            },
        ),
        migrations.CreateModel(
            name='CargoPermisos',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('cargo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.cargo')),
            ],
            options={
                'db_table': 'cargo_permisos',
            },
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'cliente',
            },
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'estado',
            },
        ),
        migrations.CreateModel(
            name='Permiso',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'permiso',
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('nombre_razon_social', models.CharField(max_length=25)),
                ('numero_documento', models.CharField(max_length=11)),
                ('hora_insercion', models.DateField()),
                ('hora_modificacion', models.DateField()),
            ],
            options={
                'db_table': 'persona',
            },
        ),
        migrations.CreateModel(
            name='Prioridad',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=25)),
            ],
            options={
                'db_table': 'prioridad',
            },
        ),
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'servicio',
            },
        ),
        migrations.CreateModel(
            name='Sexo',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'sexo',
            },
        ),
        migrations.CreateModel(
            name='TipoDocumento',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=11)),
            ],
            options={
                'db_table': 'tipo_documento',
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('contrasenha', models.CharField(max_length=8)),
                ('cargo_permisos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.cargopermisos')),
                ('persona', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.persona')),
            ],
            options={
                'db_table': 'usuario',
            },
        ),
        migrations.AddField(
            model_name='persona',
            name='sexo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.sexo'),
        ),
        migrations.AddField(
            model_name='persona',
            name='tipo_documento',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.tipodocumento'),
        ),
        migrations.CreateModel(
            name='Cola',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('hora_insercion', models.DateTimeField()),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.cliente')),
                ('estado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.estado')),
                ('servicio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.servicio')),
                ('usuario_insercion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.usuario')),
            ],
            options={
                'db_table': 'cola',
            },
        ),
        migrations.AddField(
            model_name='cliente',
            name='persona',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.persona'),
        ),
        migrations.AddField(
            model_name='cliente',
            name='prioridad_id',
            field=models.ForeignKey(db_column='id_1', on_delete=django.db.models.deletion.CASCADE, to='turnero.prioridad'),
        ),
        migrations.AddField(
            model_name='cargopermisos',
            name='permiso',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='turnero.permiso'),
        ),
    ]
