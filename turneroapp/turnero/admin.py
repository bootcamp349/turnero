from django.contrib import admin
from .forms import ClienteModelForm, PersonaModelForm
from .models import Servicio, Estado, Cargo, Permiso, CargoPermisos, TipoDocumento, Prioridad, Sexo, Persona, Cliente, Usuario, Cola


admin.site.register([Servicio, Estado, Cargo, Permiso, CargoPermisos, TipoDocumento, Prioridad, Sexo, Persona, Cliente, Usuario, Cola])


# otra forma de llenar los formularios
class ClienteAdmin(admin.ModelAdmin):
    list_display = ["persona_id", "prioridad_id"]
    list_filter = ["persona_id"]
    list_editable = ["persona_id","prioridad_id"]
    search_fields = ["persona_id"]
    form=ClienteModelForm


class PersonaAdmin(admin.ModelAdmin):
    list_display = ["nombre_razon_social", "numero_documento"]
    list_filter = ["numero_documento"]
    list_editable = ["nombre_razon_social","numero_documento","tipo_documento", "email","hora_insercion","sexo"]
    search_fields = ["numero_documento"]
    form=PersonaModelForm


# por lo que entiendo es especial para modificar los registros que ya existen

