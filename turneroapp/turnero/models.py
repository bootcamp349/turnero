# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Servicio(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=20)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'servicio'

    def __str__(self) -> str:
        return self.descripcion
    


class Estado(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=20, blank=False, null=False)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'estado'

    def __str__(self) -> str:
        return self.descripcion


class Cargo(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=20, blank=False, null=False)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
         db_table = 'cargo'

    def __str__(self) -> str:
        return self.descripcion

class Permiso(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=30, blank=False, null=False)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'permiso'

    def __str__(self) -> str:
        return self.descripcion


class CargoPermisos(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE)
    permiso = models.ForeignKey(Permiso, on_delete=models.CASCADE)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'cargo_permisos'

    # def __str__(self) -> str:
    #     return self.cargo


class TipoDocumento(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=100, blank=False, null=False)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'tipo_documento'

    def __str__(self) -> str:
        return self.descripcion


class Prioridad(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=100, blank=False, null=False)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'prioridad'

    def __str__(self) -> str:
        return self.descripcion


class Sexo(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    descripcion = models.CharField(max_length=20, blank=False, null=False)
    horaInsercion = models.DateTimeField(null=True)

    class Meta:
        db_table = 'sexo'

    def __str__(self) -> str:
        return self.descripcion


class Persona(models.Model):
    id = models.BigAutoField(primary_key=True, null=False)
    nombre_razon_social = models.CharField(max_length=100, blank=False, null=False)
    numero_documento = models.CharField(max_length=11, blank=False, null=False)
    tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE)
    email = models.EmailField(max_length=100, null=True)
    hora_insercion = models.DateField(null=True)
    sexo = models.ForeignKey(Sexo, on_delete=models.CASCADE)

    class Meta:
        db_table = 'persona'

    def __str__(self) -> str:
        return self.nombre_razon_social

class Cliente(models.Model):
    id = models.BigAutoField(primary_key=True, null=False)
    persona_id = models.ForeignKey(Persona, on_delete=models.CASCADE)
    prioridad_id = models.ForeignKey(Prioridad, on_delete=models.CASCADE, db_column='id_1')

    class Meta:
        db_table = 'cliente'

    # def __str__(self) -> str:
    #     return self.persona



class Usuario(models.Model):
    id = models.SmallAutoField(primary_key=True, null=False)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    contrasenha = models.CharField(max_length=8)
    cargo_permisos = models.ForeignKey(CargoPermisos, on_delete=models.CASCADE)

    class Meta:
        db_table = 'usuario'

    def __str__(self) -> str:
        return self.persona

class Cola(models.Model):
    id = models.BigAutoField(primary_key=True, null=False)
    usuario_insercion = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    hora_insercion = models.DateTimeField(null=True)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)


    class Meta:
        db_table = 'cola'

    def __str__(self) -> str:
        return self.persona
