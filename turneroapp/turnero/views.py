from django.db import Error
from django.shortcuts import render
from django.http import HttpResponse
from .forms import ClienteFormulario, PersonaFormulario, ClienteModelForm, PersonaModelForm, ColaModelForm
from .models import Cliente, Persona,Cola
# def index(request):
#     return HttpResponse("Pagina Principal")
#la funcion render tiene 3 parametros
    # request
    # el path del html -> la subcarpeta de templates
    # el contexto -> un diccionario que contiene las variables que va a usar el template

# no olvidar la carpeta donde estan los templates, que estan dentro de la carpeta templates

# la manera mas limpia de crear las vistas es haciendolo a travez de los templates
# pero ello conlleva un conocimiento del lenguaje html propio de django
# lo que no esta en el alcance de este programa de estudios

def index(request):
    return render(request, "turnero/index.html", {})


def cliente(request):
    form = ClienteModelForm(request.POST or None) #Al hacer esto, ya en el formulario, nos aparecen las validaciones y restricciones del mismo, puesto que lo estamos armando desde el models
    if form.is_valid():
            form_data = form.cleaned_data
            print(form_data)
            try:    
                obj = Cliente()
                obj.persona_id = form_data.get("persona_id")
                obj.prioridad_id = form_data.get("prioridad_id")
                obj.save()
            except Error as e:
                 print("Error", e)
    context = {
        'titulo':'Formulario de Clientes',
        'form':form
    }
    return render(request, "turnero/base.html", context)


def persona(request):
    form = PersonaModelForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)  
        obj = Persona()
        obj.nombre_razon_social = form_data.get("nombre_razon_social")
        obj.numero_documento = form_data.get("numero_documento")
        obj.tipo_documento = form_data.get("tipo_documento")
        obj.email = form_data.get("email")
        obj.hora_insercion = form_data.get("hora_insercion")
        obj.sexo = form_data.get("sexo")
        obj.save()
    context = {
        'titulo':'Formulario de Personas',
        'form':form
    }
    return render(request, "turnero/persona.html", context)

def cola(request):
    form = ColaModelForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)  
        obj = Cola()
        obj.usuario_insercion = form_data.get("usuario_insercion")
        obj.cliente = form_data.get("cliente")
        obj.hora_insercion = form_data.get("hora_insercion")
        obj.estado = form_data.get("estado")
        obj.servicio = form_data.get("servicio")
        obj.save()
    context = {
        'titulo':'Formulario de Cola de Clientes',
        'form':form
    }
    return render(request, "turnero/cola.html", context)

def listar_cola(request):
    cola = Cola.objects.all()
    return render(request, "turnero/lista_colas.html", {"cola":cola})