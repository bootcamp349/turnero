from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("cliente/", views.cliente, name="cliente"),
    path("persona/", views.persona, name="persona"),
    path("cola/", views.cola, name="cola"),
    path("lista_colas/", views.listar_cola, name="lista_cola")
]
